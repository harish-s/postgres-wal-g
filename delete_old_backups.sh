#!/bin/bash

export PGUSER=$POSTGRES_USER
export PGDATABASE=$POSTGRES_DB

# retains last 4 full backups
wal-g delete --confirm retain 4
