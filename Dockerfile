FROM postgres:12

ARG WALG_RELEASE=v0.2.14

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update  -qq && \
    apt-get install -qqy curl ca-certificates cron && \
    cd /usr/local/bin && curl -L https://github.com/wal-g/wal-g/releases/download/$WALG_RELEASE/wal-g.linux-amd64.tar.gz | tar xzf -
RUN apt-get -y install postgresql-12-cron

ADD fix-acl.sh /docker-entrypoint-initdb.d/
ADD setup-walg.sh /docker-entrypoint-initdb.d/

COPY make_base_backup.sh /
COPY delete_old_backups.sh /
RUN echo "@daily bash /make_base_backup.sh $PGDATA " | crontab -
RUN crontab -l | { cat; echo "0 20 4 * 0 bash /delete_old_backups.sh"; }| crontab -