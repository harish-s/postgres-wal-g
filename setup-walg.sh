#!/bin/bash

# Verify required environment variables are set
: "${AWS_ACCESS_KEY_ID:?AWS_ACCESS_KEY_ID does not exist}"
: "${AWS_SECRET_ACCESS_KEY:?AWS_SECRET_ACCESS_KEY does not exist}"
: "${WALG_S3_PREFIX:?WALG_S3_PREFIX does not exist}"


# Assumption: the group is trusted to read secret information
umask u=rwx,g=rx,o=

# Setup the archive wal-e configuration
echo "wal_level = archive" >> /var/lib/postgresql/data/postgresql.conf
echo "archive_mode = on" >> /var/lib/postgresql/data/postgresql.conf
echo "archive_command = 'wal-g wal-push %p'" >> /var/lib/postgresql/data/postgresql.conf
echo "archive_timeout = 60" >> /var/lib/postgresql/data/postgresql.conf

#Setup pg_cron extension
echo "shared_preload_libraries='pg_cron'" >> /var/lib/postgresql/data/postgresql.conf
echo "cron.database_name='${POSTGRES_DB}'" >> /var/lib/postgresql/data/postgresql.conf

# Restore from the latest backup if RESTORE is set
if [ "$RESTORE" != "" ]; then
  if [ ! -f /var/lib/postgresql/data/recovery.done ]; then
    pg_ctl -D "$PGDATA" -m immediate stop
    rm -rf $PGDATA
    wal-g backup-fetch /var/lib/postgresql/data/ LATEST
    echo "restore_command = 'wal-g wal-fetch \"%f\" \"%p\"'" >> /var/lib/postgresql/data/postgresql.conf
    chown postgres:postgres /var/lib/postgresql/data/postgresql.conf
    touch /var/lib/postgresql/data/recovery.signal

    pg_ctl -D "$PGDATA" -o "-c listen_addresses=''" start -t 600

EOSQL
    echo "Complete"
  fi
fi
