# Postgres WAL_G

This image extends the stock [Postgres Image](https://hub.docker.com/_/postgres/)
and adds [WAL-G](https://github.com/wal-g/wal-g) support, along with some
helpful scripts. It allows you to simply and easily create continuous 
archives of Postgres databases in S3-compatible storage.

**Note:** Update the image name in circleCI env variable(POSTGRES_IMAGE) if you are building the image with new tag name 

## Building the image

You can pass the WAL_G version which we want to use for building the image. Default value is v0.2.14

```
docker build --build-args WALG_RELEASE=<latest_release_version> -t <repo/postgres-wal-g> .
```

## Push the image

```
docker push <repo/postgres-wal-g>
```

## Environment
To use the image, you'll need to configure a bucket on your favorite S3-
compatible provider, and set a few environment variables:

* `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` that can read/write files
to your bucket
* `WALG_S3_PREFIX` to tell WAL-G which bucket and prefix you'd like to use
* `RESTORE` to tell whether we want to restore the database from most recent backup(fetched using WALG_S3_PREFIX). Just set this variable with some value.
## Full Backups

This will happen daily once you've started the container.

To run a full backup manually, simply run `/make_base_backup.sh [postgres_data_directory]` after you login to the container.

## Continuous Backups
These will happen automatically once you've started container.